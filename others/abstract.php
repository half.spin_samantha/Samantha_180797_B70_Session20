<?php

abstract class MyAbstractClass{

     abstract public function askName();
     abstract public function askPhone();

    public function doSomething(){

        echo "I'm doing Something<br>";
    }
}

class Person extends MyAbstractClass{


    public function askName()
    {
        // TODO: Implement askName() method.
    }


    public function askPhone()
    {
        // TODO: Implement askPhone() method.
    }
}