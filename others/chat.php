<?php
if(!isset($_SESSION)) session_start();

if(isset($_POST['msg']) && !empty($_POST['msg'])){

    $str =  "<".$_SESSION['chatNick']."> : " . $_POST['msg'] . "\n";

    file_put_contents("conversation.txt",$str,FILE_APPEND);

}

if(isset($_POST['chatNick'])){

    $_SESSION['chatNick'] = $_POST['chatNick'];
}

if(!isset($_SESSION['chatNick']) || empty($_SESSION['chatNick'])){

    echo '
        
        <form action="" method="post">
            
           Please enter you chat nick : 
           <input type="text" name="chatNick">
           <input type="submit">
        
        </form>      ';
}

else {

    echo "Welcome " . $_SESSION['chatNick'] . " !<br>";

    echo "<a href='logout.php'>Logout</a> <br>";

    $conversation = file_get_contents("conversation.txt");

    echo "<textarea id='chatHistory' rows='20' cols='150'> $conversation</textarea>";

    echo '
        
        <form action="" method="post">
            
           <input type="text" name="msg">
           <input type="submit" name="send">
        
        </form>   ';
}
?>

    <script>

    setInterval(function() {
        loadDoc ();
        document.getElementById("chatHistory").scrollTop =  document.getElementById("chatHistory").scrollHeight-200;
    },1000);

    function loadDoc() {
        var xhttp;

        if (window.XMLHttpRequest) {
            // code for modern browsers
            xhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("chatHistory").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "conversation.txt", true);
        xhttp.send();
    }
</script>

