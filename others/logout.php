<?php

if (!isset($_SESSION)) session_start();

session_destroy();

header("Location: chat.php"); //redirects a file to the previous one