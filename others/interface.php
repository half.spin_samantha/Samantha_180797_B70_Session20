<?php

interface CanFly{

    public function fly();
}

interface CanSwim{

    public function swim();

}



class Bird{

    public function inf(){

        echo "I am a Bird.<br> I'm {$this->name} ";
    }
}

class Penguin extends Bird implements CanSwim{

    public $name = "Penguin";

    public function swim()
    {
        // TODO: Implement swim() method.
        echo "I can swim <br>";
    }


}

class Duck extends Bird implements CanFly{

    public $name = "Duck";

   public function fly()
   {
       // TODO: Implement fly() method.
       echo "I can fly <br>";



   }

}

class Dove extends Bird implements CanFly{

    public $name = "Dove";

    public function fly()
    {
        // TODO: Implement fly() method.
        echo "I can fly <br>";

    }

    public function swim()
    {
        // TODO: Implement swim() method.
        echo "I can swim <br>";

    }

}

describe(new Penguin());
describe(new Dove());
describe(new Duck());

function describe($objBird){

    //$objBird->info();

    if($objBird instanceof CanFly){

        $objBird->fly();
        echo "I am a ".$objBird->name. " <br>";
    }

    if($objBird instanceof CanSwim){

        $objBird->swim();
        echo "I am a ".$objBird->name. " <br>";
    }
}