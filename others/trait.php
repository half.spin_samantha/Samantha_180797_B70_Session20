<?php

trait A{

    public function tell(){
        echo "I am A<br>";
    }
}

trait B{

    public function tell(){
        echo "I am B<br>";
    }

}

class C{

    use A, B;
}

$objC = new C();
$objC->tell();