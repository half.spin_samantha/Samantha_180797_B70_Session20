<?php

class StudentInformation {

    protected $_stdID;
    protected $_stdName;
    protected $_stdDOB;
    protected $_stdGender;
    protected $_stdBanglaMark;
    protected $_stdEnglishMark;
    protected $_stdMathMark;
    protected $_stdICTMark;

    public function setFormData($_formInfo) {

        if(array_key_exists("ID",$_formInfo)){
            $this->_stdID = $_formInfo["ID"];
        }
        if(array_key_exists("Name",$_formInfo)){
            $this->_stdName = $_formInfo["Name"];
        }
        if(array_key_exists("DOB",$_formInfo)){
            $this->_stdDOB = $_formInfo["DOB"];
        }
        if(array_key_exists("Gender",$_formInfo)){
            $this->_stdGender = $_formInfo["Gender"];
        }
        if(array_key_exists("BanglaMark",$_formInfo)){
            $this->_stdBanglaMark = floatval($_formInfo["BanglaMark"]);
        }
        if(array_key_exists("EnglishMark",$_formInfo)){
            $this->_stdEnglishMark = floatval($_formInfo["EnglishMark"]);
        }
        if(array_key_exists("MathMark",$_formInfo)){
            $this->_stdMathMark = floatval($_formInfo["MathMark"]);
        }
        if(array_key_exists("ICTMark",$_formInfo)){
            $this->_stdICTMark = floatval($_formInfo["ICTMark"]);
        }

    }

    public function displayStudentID()
    {
        return $this->_stdID;
    }
    public function displayStudentName()
    {
        return $this->_stdName;
    }
    public function displayStudentDOB()
    {
        return $this->_stdDOB;
    }
    public function displayStudentGender()
    {
        return $this->_stdGender;
    }
    public function displayStudentBanglaMark()
    {
        return $this->_stdBanglaMark;
    }
    public function displayStudentEnglishMark()
    {
        return $this->_stdEnglishMark;
    }
    public function displayStudentMathMark()
    {
        return $this->_stdMathMark;
    }
    public function displayStudentICTMark()
    {
        return $this->_stdICTMark;
    }
    public function displayGradeFunction($_gradeFunction)
    {
        if($_gradeFunction<=100 && $_gradeFunction>=80){
            echo "<b>A<sup>+</sup></b>";
        }elseif ($_gradeFunction<=79 && $_gradeFunction>=70){
            echo "<b>A</b>";
        }elseif ($_gradeFunction<=69 && $_gradeFunction>=60){
            echo "<b>A<sup>-</sup></b>";
        }elseif ($_gradeFunction<=59 && $_gradeFunction>=50){
            echo "<b>B</b>";
        }elseif ($_gradeFunction<=49 && $_gradeFunction>=40){
            echo "<b>C</b>";
        }elseif ($_gradeFunction<=39 && $_gradeFunction>=33){
            echo "<b>D</b>";
        }else {
            echo "<b class=\"fGrade\">F</b>";
        }
    }
    public function displayFinalResult()
    {
        if($this->_stdBanglaMark>=33 && $this->_stdBanglaMark<=100 && $this->_stdEnglishMark>=33 && $this->_stdEnglishMark<=100 && $this->_stdMathMark>=33 && $this->_stdMathMark<=100 && $this->_stdICTMark>=33 && $this->_stdICTMark<=100){
            echo '<span class="pass">Pass</span>';
        }else{
            echo '<span class="fail">Fail</span>';
        }
    }

}

$objStudentInformation = new StudentInformation;

$objStudentInformation->setFormData($_POST);

?>